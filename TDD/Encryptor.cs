﻿using System;
using System.Linq;

namespace TDD
{
    public class Encryptor
    {
        public string CryptWord(string word)
        {
            if (word.Contains(" "))
            {
                throw new ArgumentException();
            }

            char[] wordArray = word.ToCharArray();
            string newWord = "";
            for (int i = 0; i < word.Length; i++)
            {
                int charValue = wordArray[i];
                newWord += (char)(charValue + 2);
            }

            return newWord;
        }

        public string CryptWordToNumbers(string word)
        {
            if (word.Contains(" "))
            {
                throw new ArgumentException();
            }

            char[] wordArray = word.ToCharArray();
            string newWord = "";
            for (int i = 0; i < word.Length; i++)
            {
                int charValue = wordArray[i];
                newWord += charValue + 2;
            }

            return newWord;
        }

        public string CryptWord(string word, string charsToReplace)
        {
            if (word.Contains(" "))
            {
                throw new ArgumentException();
            }

            char[] wordArray = word.ToCharArray();
            char[] replacement = charsToReplace.ToCharArray();
            char[] result = wordArray.ToArray();
            for (int i = 0; i < wordArray.Length; i++)
            {
                for (int j = 0; j < replacement.Length; j++)
                {
                    if (replacement[j] == wordArray[i])
                    {
                        int charValue = wordArray[i];
                        result[i] = (char)(charValue + 2);
                    }
                }
            }
            return new string(result);
        }

        public string CryptSentence(string sentence)
        {
            char[] sentenceArray = sentence.ToCharArray();
            string newWord = "";
            for (int i = 0; i < sentence.Length; i++)
            {
                int charValue = sentenceArray[i];
                newWord += (char)(charValue + 2);
            }

            return newWord;
        }

        public string[] GetWords(string sentence)
        {
            return sentence.Split(' ');
        }

        public void PrintWords(string sentence)
        {
            string[] words = GetWords(sentence);
            foreach (var word in words)
            {
                Console.WriteLine("<" + word + ">");
            }
        }

    }
}
